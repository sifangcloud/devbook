# 使用 IDEA

1. [IDEA Java 快捷键](IdeaJ_keyshouts.md)
1. [IDEA Java  插件](IdeaJ_plugin.md)
1. [IntelliJ IDEA官方专业文档](https://www.w3cschool.cn/intellij_idea_doc/intellij_idea_doc-k9g62o2z.html)


## 自定义头注释模板

### PyCharm
file -->settings–>editor–>file and code templates

```
#!/usr/bin/env python
# _*_coding:utf-8_*_

"""
@Time : ${DATE} ${TIME}
@Author: ${USER}
@File: ${NAME}.py
@Project: ${PRODUCT_NAME}
"""
```

其他

```
${PROJECT_NAME} - 当前Project名称; (the name of the current project. )
${NAME} -创建文件的对话框中制定的文件名; (the name of the new file which you specify in the New File dialog box during the file creation. )
${USER} - 当前用户名; the login name of the current user.
${DATE} - 当前系统日期; the current system date.
${TIME} - 当前系统时间; the current system time.
${YEAR} - 年; the current year.
${MONTH} - 月; the current month.
${DAY} - 日; the current day of the month.
${HOUR} - 小时; the current hour.
${MINUTE} - 分钟; the current minute.
${PRODUCT_NAME} - 创建文件的IDE名称; the name of the IDE in which the file will be created.
${MONTH_NAME_SHORT} - 英文月份缩写; the first 3 letters of the month name. Example: Jan, Feb, etc.
${MONTH_NAME_FULL} - 英文月份全称; full name of a month. Example: January, February, etc.
```

## 常用快捷键

```
Alt+Enter   导入包,自动修正
Ctrl+N      查找类
Ctrl+Shift+N  查找文件
Ctrl+F , Alt＋F3  查找文本
Ctrl+Shift+F 在任意路径查找文本,可以使用通配符，也可以使用缩写
Ctrl+R 替换文本
Ctrl+Shift+R 在任意路径替换文本
Ctrl+X 剪切行
Ctrl+D 复制行
Ctrl+Y 删除一行
Ctrl＋Shift＋J    可以整合两行
Ctrl+/ 或 Ctrl+Shift+/  注释（// 或者/*...*/ ）
sout System.out.println() 
Ctrl+Shift+ ↑  上移一行
Ctrl+Shift+ ↓  下移一行
Ctrl+Alt+L  格式化代码
Ctrl+Alt+O 优化导入的类和包
Ctrl+P 方法参数提示
Ctrl+Q 显示注释文档(JavaDoc)
Ctrl＋F12  显示当前文件的结构
Ctrl＋[或]可以跳到大括号的开头结尾
Ctrl＋Alt＋T   把代码包在一块内，例如try/catch
Ctrl+Shift+Up/Down 代码向上/下移动
Alt+ Up/Down 在方法间移动
Alt＋Enter  引入包提示
Alt+Insert 生成代码(如get,set方法,构造函数等)
Ctrl+Shift+Space 代码补全Smart提示
Ctrl+Alt+Space 类名或接口名自动完成
Ctrl+J  自动补全代码
Alt+F1 在各个面板中定位打开页面
Shift+F6  重构-重命名
Ctrl+Alt+ ←/→  返回至 上次/下次 浏览的位置
Alt+ ←/→       切换代码页面
Ctrl＋O    选择父类的方法进行重写
F2 或Shift+F2  高亮错误或警告快速定位
Ctrl+W        选中代码，连续按会有选中更多的块
Alt＋F7  查找使用类、方法或者变量的位置
选中文本，按Ctrl+Shift+F7 ，高亮显示所有该文本，按Esc高亮消失。
选中文本，按F3 ，逐个往下查找相同文本，并高亮显示。
Ctrl+H    显示类结构图
Alt＋Q    当前方法的声明
Ctrl+E       显示最近编辑的文件列表
Ctrl＋Shift＋Backspace    可以跳转到上次编辑的地方
Ctrl+E 或者 Alt+Shift+C   最近更改的代码
Alt+1       快速打开或隐藏工程面板

Ctrl+B      打开光标处的类或方法的声明
Ctrl＋Alt＋B    跳转到抽象方法的实现

Shift＋Click    关闭文件
Ctrl+Shift+Alt+N 查找类中的方法或变量
Alt+Shift+C  对比最近修改的代码
代码标签输入完成后，按Tab，生成代码。

Ctrl＋F7     查询当前元素在当前文件中的引用，然后按F3可以选择
Ctrl＋Shift＋Insert可以选择剪贴板内容并插入
Ctrl＋Alt＋V   引入变量。例如把括号内的SQL赋成一个变量
```

## IDEA 好用插件

### IDEA Java

1. git工具
   1. 自带的 git 插件
   2. [gittoolbox](https://plugins.jetbrains.com/plugin/index?xmlId=zielu.gittoolbox): 代码中显示提交信息，仓库上显示提交落后数
2. Maven工具
    * [Maven Helper](https://plugins.jetbrains.com/plugin/7179-maven-helper/)
3. 实体GET/SET
    * [Lombok](https://plugins.jetbrains.com/plugin/6317-lombok/)
4. 代码/Bug静态检查
	+ [CheckStyle-IDEA](https://plugins.jetbrains.com/plugin/1065-checkstyle-idea/) , [配置文件](checkstyle.xml) [下载](https://checkstyle.sourceforge.io/checks.html)
	+ [Alibaba Java Coding Guidelines](https://plugins.jetbrains.com/plugin/10046-alibaba-java-coding-guidelines/)
	+ [Eclipse Code Formatter](https://plugins.jetbrains.com/plugin/6546-eclipse-code-formatter/)
	+ [FindBugs-IDEA](https://plugins.jetbrains.com/plugin/3847-findbugs-idea/) [配置文件设置](http://findbugs.sourceforge.net/bugDescriptions.html) : 检查对象为字节码
	+ [PMDPlugin](https://plugins.jetbrains.com/plugin/1137-pmdplugin/) [配置文件设置](https://pmd.github.io/pmd-6.15.0/pmd_rules_java.html) : 检查对象为源代码
	+ [Cobertura]() : 白盒测试，UT覆盖率测试
	+ [Save Actions](https://plugins.jetbrains.com/plugin/7642-save-actions/) , 按照规则自动检查优化代码
4. MyBatis/Plus
    * 代码生成
      + [Mybatis Builder](https://plugins.jetbrains.com/plugin/12449-mybatis-builder/versions)(适用IdeaJ 2019后的版本): 通过IDEAJ连接数据库并将 Mybatis Generator 的功能图形化,生成Mybatis的 Mapper/Entity/SqlMap.xml 的文件。
      + [Build Mybatis File](https://github.com/moshangren11/MybatisPlugin): 和[Mybatis Builder](https://plugins.jetbrains.com/plugin/12449-mybatis-builder/versions) 相同，没那么好用，但可以兼容IdeaJ 2019前的版本。
      + Mybatis Generator 不需要图形界面，可以作为 maven 插件嵌入。没有 Mybatis Builder 好用
    * Java 与 XML 调回跳转；Mapper 方法自动生成 XML
      + [MyBatisX](https://baomidou.com/guide/mybatisx-idea-plugin.html) 插件[地址](https://plugins.jetbrains.com/plugin/10119-mybatisx/)
      + [Free MyBatis plugin](https://plugins.jetbrains.com/plugin/8321-free-mybatis-plugin/):
        - 生成mapper xml文件
        - 快速从代码跳转到mapper及从mapper返回代码
        - mybatis自动补全及语法错误提示
        - 集成mybatis generator gui界面
5. Yaml文件
   * swagger, yaml 文件分级显示, [插件地址](https://plugins.jetbrains.com/plugin/index?xmlId=org.zalando.intellij.swagger)
   * YAML/Ansible support, YAML规范提示工具, [插件地址](https://plugins.jetbrains.com/plugin/7792-yaml-ansible-support)
6. 其他:	
	* Rainglow Color Schemes,通过不同颜色进行括号对匹配，快速帮助定位代码片段,[插件地址](https://plugins.jetbrains.com/plugin/10267-rainglow-color-schemes/versions)
    * String Manipulation, 进行代码中的变量快速处理，进行Camel、snake等各种格式快速处理, [插件地址](https://plugins.jetbrains.com/plugin/2162-string-manipulation/versions)
    * Jprofiler,Java性能分析工具, [插件地址](https://plugins.jetbrains.com/plugin/253-jprofiler)
    * Easy Code，在idea里面选择数据库表，全部搞定CRUD。 基于IntelliJ IDEA开发的代码生成插件，支持自定义任意模板（Java，html，js，xml）。只要是与数据库相关的代码都可以通过自定义模板来生成。支持数据库类型与java类型映射关系配置。支持同时生成生成多张表的代码。每张表有独立的配置信息。完全的个性化定义，规则由你设置。
7. 编程语言
   * bashsupport, 支持在IDEA中编写shell脚本文件，有友好的代码格式，支持自动补全，检查错误，并且配置完之后，还可以在IEDA中直接运行shell脚本
8. 画图
   * UML Support,专业版内置插件，可以生成UML类图
   * SequenceDiagram, 根据代码生成时序图

## 前端

### VUE

1. [IntelliVue](https://plugins.jetbrains.com/plugin/12014-intellivue/)
2. [Vue.js](https://plugins.jetbrains.com/plugin/9442-vue-js/)

## 其他

1. [leetcode editor](https://plugins.jetbrains.com/plugin/12132-leetcode-editor/) 
