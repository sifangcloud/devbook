# 华为云OBS

## 初始化
obsutil config -i=ak -k=sk -e=endpoint

## 快速入门
创建桶
```
obsutil mb obs://bucket-test -location=cn-south-1
```

将本地test.txt文件上传至bucket-test桶中
```
obsutil cp /temp/test.txt obs://bucket-test/test.txt
```

将bucket-test桶中的test.txt对象下载至本地
```
obsutil cp obs://bucket-test/test.txt /temp/test1.txt
```

在bucket-test桶中删除test.txt对象
```
obsutil rm obs://bucket-test/test.txt -f
```

删除bucket-test桶
```
obsutil rm obs://bucket-test -f
```

## 上传对象

将D盘temp路径下名为test.txt的文件上传至bucket-test桶中，并且重命名为key
```
obsutil cp d:\temp\test.txt obs://bucket-test/key
```

递归上传D盘temp文件夹中的所有文件和子文件夹至bucket-test桶的temp文件夹中
```
obsutil cp d:\temp obs://bucket-test -f -r
```

## 其他

obs 文件覆盖验证

```
obsutil config -i=ak -k=sk -e=endpoit
obsutil sync obs://testobs/b/ obs://testobs/a/
obsutil cp obs://testobs/b/1.pdf obs://testobs/a/1.pdf
```
- 这是一种覆盖，无论b目录是否比a目录新都会覆盖。
- 同步时将会进行递归复制。

## 参考 <!-- {docsify-ignore} -->
1. [华为云对象存储服务 OBS帮助文档](https://support.huaweicloud.com/obs/index.html)
2. [华为云OBS工具汇总](https://support.huaweicloud.com/tg-obs/obs_09_0001.html)
3. [官方上传demo](https://support.huaweicloud.com/sdk-java-devg-obs/obs_21_0602.html)