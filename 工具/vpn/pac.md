# PAC自动代理文件

## PAC文件格式

PAC文件是纯文本格式的，实际上就是JavaScript文件。Chrome/Chromium的扩展Switchy!的"Auto Switch Mode"功能实际上也是创建和维护一个简单的PAC文件，但功能比较弱。

对于一般的应用，即使你几乎不懂JavaScript和编程，也可以通过本文的介绍实现基本的功能。

### PAC文件FindProxyForURL函数 

PAC文件中必须包含一个函数：FindProxyForURL(url, host)。

参数url是用户输入的url，参数host是url中的主机名。

比如url为http://www.truevue.org/javascript/pac-proxy-setting，那么host就是www.truevue.org

一个最简单的PAC文件内容如下：

```
function FindProxyForURL(url, host) {
 return "DIRECT"; 
} 
```

这个PAC文件实际上什么也没做，对任何URL，都将"DIRECT"（直接连网）。

## PAC文件返回值类型

除了可以return "DIRECT"以外，还有两种常用方式：

```
PROXY proxysample.com:8080
```

http代理的主机和端口，主机也可以用IP表示

```
SOCKS5 socks5sample.com:1080
```

socks5代理的主机和端口，主机也可以用IP表示

那么，我们可以猜测到，用pac指定一个http代理应该这样写

```
function FindProxyForURL(url, host) {
  return "PROXY 192.168.1.1:3128"; 
}
```

甚至可以指定多个代理

```
function FindProxyForURL(url, host) {
  return "DIRECT; PROXY 192.168.1.1:3128; SOCKS5 lilinux.net:1080"; 
} 
```

这句语句的意思是： 

1. 对所有URL，都直接连接； 
2. 如果不能直接连接，那么就使用192.168.1.1:3128这个http代理连接；
3. 如果还是不能连接，则使用lilinux.net:1080这个socks5代理连接。

使用不同连接的顺序和语句中的顺序一致，你可以根据自己的实际情况更改。

也许你明确知道哪些网站不能直连，必须用PROXY或者SOCKS5连接，那么可以对站点分别指定代理配置 

```
function FindProxyForURL(url, host) {
   if (shExpMatch(url,"*.google.com/*")) {
     return "PROXY 192.168.1.1:3128";
   }
   if (shExpMatch(url, "*.wikipedia.com:*/*")) {
     return "SOCKS5 lilinux.net:1080";
   }
   if (isInNet(host, "10.0.0.0",  "255.0.0.0")){
     return "DIRECT";
   }
   return "DIRECT; PROXY 192.168.1.1:3128; SOCKS5 lilinux.net:1080"; 
}
```

这个PAC文件中引入了两个新的函数，但从字面意思上，我们也可以猜出代码的大概意思：

1. 当url是*.google.com/* 时，自动使用PROXY代理；
2. 当url是*.wikipedia.cm/*时，自动使用SOCKS5代理；
3. 当host是10.0.0.0 /255.0.0.0的子网内时，自动直连；
4. 如果都不匹配，则依次按DIRECT、PROXY、SOCKS5的次序尝试。 

shExpMatch函数用来匹配url或者host，匹配的方式和DOS的通配符相似。例如前面用到的"*.google.com/*"可以匹配任意包含".google.com/"的字符串。 

Chrome/Chromium 的扩展Switchy!创建的pac文件还自定义了一个函数，可以用来匹配正则表达式，不过个人认为在url匹配上通常不需要使用强大的正则表达式。 

isInNet函数用来返回请求的host是否在指定的域内。值得注意的是，isInNet的第二个参数必须是 IP，不能是主机名。因此需要把主机名转换成IP。比如"isInNet(host, dnsResolve([www.google.com](http://www.google.com/)), "255.255.255.0")"讲到这里，应该可以解决你的问题了吧。

### PAC文件可以使用的JavaScript函数

当然PAC也不止这么简单，它还提供了不少其它函数，在本文就不详细讲述了。http://www.truevue.org/javascript/pac-functions 中列出了PAC代理文件中可以使用的JavaScript函数。

你也许想把pac文件发布到Internet上，这样其它用户就只需要在浏览器中指定pac文件的url即可。你得配置你的服务器映射 .pac 文件后缀到MIME类型： application/x-ns-proxy-autoconfig 如果使用的是Netscape服务器，编辑 config 目录下的 mime.types 文 件。如果是Apache, CERN or NCSA服务器，使用 AddType 指令。



## 佛跳墙设置pac

# 代理设置失败教程

一、打开IE浏览器，在右上角点击工具（有些系统会是齿轮图标）->Internet选项

![img](pics/001.png)

二、选中 连接->局域网设置

![img](pics/002.png)

三、勾选 使用自动配置脚本； 复制佛跳墙首页上地址栏上的内容 把最后的connect修改为fei.pac（比如如：http://localhost:53111/js6pgiklihgp589pflrtwf7bt5j2whuy/fei.pac，这只是个例子，一定要复制您电脑上佛跳墙首页上的地址），填写到“局域网设置”中的 “地址” 上；只保留如图的配置，其他一律去掉。

![img](pics/003.png)

![img](pics/004.png)



注意：由于这个地址是每次启动加速器都会变化，所以如果您之后启动加速器或仍然上不了网，步骤三都要重新配置配置一遍

佛跳墙pac设置参考 

```
function FindProxyForURL(url, host) 
{
	if (shExpMatch(url,"*.google.*")) 
	{
		return "PROXY 127.0.0.1:2800";
	}
	return "DIRECT";
}
```

