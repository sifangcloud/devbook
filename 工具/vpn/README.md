# VPN

## 访问Google
1. 访问google、facebook等: 用[佛跳墙vpn](https://getftq.internetedu.xyz/)
   1. 使用佛跳墙vpn，如需自己指定pac文件，参考 [PAC自动代理文件.md](pac.md)
2. 仅仅访问google: 
   1. 用[google助手](https://www.iggzs.com/): 需要修改主页为 hao123 才能免费试用，比较烦
   2. 用[iguge](https://iguge.xyz/): 提供谷歌学术搜索，Chrome Store等产品加速访问.会自动修改修改主页为 hao123

## 访问github加速

1. [9种方法让你访问Github提速到2MB/s](https://cloud.tencent.com/developer/article/1686178)
   
## 几种好用的VPN
1. [迷雾通](https://geph.io/zhs) 需翻墙
2. [白鲸加速器](https://www.bjchuhai.com/)  需翻墙
3. 自建VPN
   1. 安卓客户端使用：[v2rayNG](https://v2rayng.org/)
   2. Mac客户端使用： [Qv2ray](https://github.com/Qv2ray/Qv2ray/releases)
   3. Windows客户端使用： [v2rayN-With-Core]([v2rayng](https://github.com/2dust/v2rayN/releases))
4. [VPNGate](https://www.vpngate.net/cn/): 许多由世界各地的志愿者所提供的 VPN 服务器。