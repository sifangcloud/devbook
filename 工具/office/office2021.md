# Office2021

Office2021 KMS激活，纯命令行激活，不需要破解工具，没有中毒风险！

1.安装文件下载地址：

[https://pan.baidu.com/s/1KUsvxkG13uDKZL_uUrKMCQ](https://link.orcy.net.cn/?url=https://pan.baidu.com/s/1KUsvxkG13uDKZL_uUrKMCQ) 提取码：2021

Onedrive：[https://alumnialbany-my.sharepoint.com/:f:/g/personal/mlj_alumni_albany_edu/Ej_Pxw_VUKFJmDe-Dgsn43EBg4QStSii4ajw66MinvEYyg?e=fYgjXF](https://link.orcy.net.cn/?url=https://alumnialbany-my.sharepoint.com/:f:/g/personal/mlj_alumni_albany_edu/Ej_Pxw_VUKFJmDe-Dgsn43EBg4QStSii4ajw66MinvEYyg?e=fYgjXF)

解压后安装vl版office2021，`标准版`和`专业增强版`均可通过KMS激活

2.KMS激活

以管理员身份运行CMD或者Powershell，输入以下命令

```c
cd "C:/Program Files\Microsoft Office\Office16"
cscript ospp.vbs /sethst:kms.0t.net.cn
cscript ospp.vbs /setprt:1688
cscript ospp.vbs /act
```

等待激活完成即可！

![Office2021安装文件下载和KMS激活（不需要工具）-下一朵云](office2021.assets/20211118143419.png)

**适用于 LTSC 2021 Office GVLK**

| **Product**                       | **GVLK**                      |
| --------------------------------- | ----------------------------- |
| Office LTSC 专业增强版 2021       | FXYTK-NJJ8C-GB6DW-3DYQT-6F7TH |
| Office LTSC 标准版 2021           | KDX7X-BNVR8-TXXGX-4Q7Y8-78VT3 |
| Project Professional 2021 年 1 月 | FTNWT-C6WBT-8HMGF-K9PRX-QV9H8 |
| Project Standard 2021             | J2JDC-NJCYY-9RGQ4-YXWMH-T3D4T |
| VisioLTSC Professional 2021       | KNH8D-FGHT4-T8RK3-CTDYJ-K2HT4 |
| VisioLTSC Standard 2021           | MJVNY-BYWPY-CWV6J-2RKRT-4M8QG |
| Access LTSC 2021                  | WM8YG-YNGDD-4JHDC-PG3F4-FC4T4 |
| ExcelLTSC 2021                    | NWG3X-87C9K-TC7YY-BC2G7-G6RVC |
| OutlookLTSC 2021                  | C9FM6-3N72F-GFJXB-TM3V9-T86R9 |
| PowerPointLTSC 2021               | TY7XF-NFRBR-KJ44C-G83KF-GX27K |
| PublisherLTSC 2021                | 2MW9D-N4BXM-9VBPG-Q7W6M-KFBGQ |
| Skype for BusinessLTSC 2021       | HWCXN-K3WBT-WJBKY-R8BD9-XK29P |
| Word LTSC 2021                    | TN8H9-M34D3-Y64V9-TR72V-X79KV |