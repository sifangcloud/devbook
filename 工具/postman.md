# POSTMAN


## 常用脚本
### 提取接口返回值
 
1.当返回值是返回JSON时
```
let json = JSON.parse(responseBody);  // responseBody是包含整个返回内容的字符串
let foobar = json.foo.bar[0].foobar;  //提取某字段的值：  假设结构为 {"foo": {"bar": [{"foobar": 1}, {"baz": 2}]}}
```

2.当返回值是返回HTML时
方法一：用正则表达式匹配
```
let foo = responseBody.match(/foo/g);   // g 全局 i 不分大小写 m 多行 tests['blahblahblah'] = foo[0] === 'bar'; // 正则里包含变量时： let foo = 'xxx'; let bar = responseBody.match(new RegExp(`^${foo}.*$`, 'g');
```
方法二（比较笨拙）
用CheerioJS库（可以用jQuery语法）： 
```
const $ = cheerio.load(responseBody); $.html() // 整个网页 $('title').text(); // <title>标签里的文字
```

### 动态设置变量

1.设置环境变量
```
postman.setEnvironmentVariable("key", "value");
pm.environment.get("key");//postman  5.0以上版本设置环境变量的方法
pm.environment.unset("variable_key"); //清除环境变量
```

2.设置全局变量
```
postman.setGlobalVariable("key", "value");
pm.globals.set("variable_key", "variable_value"); //postman 5.0以上版本设置全局变量方法
pm.globals.get("variable_key");   //获取全局变量
pm.globals.unset("variable_key"); //清除全局变量
```

3.获取Header
```
//将token提取出来并存到变量中
var data = postman.getResponseHeader("Access-Token");
//将上面的变量设置成环境变量
pm.environment.set("Access-Token",data);
```

### 调试中常用的方式

1.ctrl+alt+C 打开postman控制台
```
console.log(foobar);  //在控制台输出获取的结果
```

4.检测JSON中的某个值是否等于预期的值

```
var data = JSON.parse(responseBody); tests["Your test name"] = data.value === 100; //如果通过postman里面会有绿色标识
```

## 参考 <!-- {docsify-ignore} -->
1. https://www.cnblogs.com/unknows/p/10032015.html
2. https://www.cnblogs.com/kristin/p/10328878.html
